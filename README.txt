README.txt
==========
Objective
=========
Destaque creates a block with content from taxonomy terms. It selects the
vacabulary based on its weight, and the terms also according to their weights
inside the vocabulary. There is also a themes folder you can use for
creating custom templates. 

USAGE
=====
Enable the module and Destaque Block will be available at
admin/structure/block. Set the vocabulary that will be used for retrieving
terms for the block. In admin/structure/taxonomy drag and drop your choosen
vocabulary to the top of the list. All the parent terms of the vacubulary will
shown up in the block. You can also change the position of the terms by their
weights inside the vocabulary.

AUTHOR
======
Rodrigo Panchiniak Fernandes <rodrigo at taller DOT net DOT br>
http://www.cce.ufsc.br/~fernandesrp
